package com.effylab.test.service.impl;

import com.effylab.test.service.api.HelloService;

/**
 * Some description...
 * <p>
 * @author Artem Labazin, xxlabaza@gmail.com
 * <p>
 * @since Jul 28, 2014
 * <p>
 * @version 1.0.0
 */
public class HelloServiceImpl implements HelloService {

    public void initialize () {
        System.out.println("Hello service initialized!");
    }

    @Override
    public String sayHello () {
        return "Hello!";
    }
}
