package com.effylab.test.client;

import com.effylab.test.service.api.HelloService;

public class HelloServiceClient {

    private HelloService helloServiceObject;

    public void initialize () {
        System.out.println("Initializing client...");
        String helloString = helloServiceObject.sayHello();
        System.out.println(helloString);
        System.out.println("Client is initialized!");
    }

    public HelloService getHelloServiceObject () {
        return helloServiceObject;
    }

    public void setHelloServiceObject (HelloService helloServiceObject) {
        this.helloServiceObject = helloServiceObject;
    }
}
