package com.effylab.test.service.api;

/**
 * Some description...
 * <p>
 * @author Artem Labazin, xxlabaza@gmail.com
 * <p>
 * @since Jul 28, 2014
 * <p>
 * @version 1.0.0
 */
public interface HelloService {

    String sayHello ();
}
